/**
 * Created by Uladzislau on 09.10.15.
 */
'use strict';

var gulp = require('gulp'),
    minifyCss = require('gulp-minify-css'),
    sass = require('gulp-sass'),
    watch = require('gulp-watch'),
    spritesmith = require('gulp.spritesmith');

gulp.task('default', function () {
    return gulp.src('src/scss/common.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(minifyCss())
        .pipe(gulp.dest('app/css'));
});

gulp.task('gulpDev', function () {
    return gulp.src('src/scss/common.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('app/css'));
});

gulp.task('sprite', function () {
    var spriteData = gulp.src('src/sprite/*.png').pipe(spritesmith({
        imgName: 'sprite.png',
        cssName: 'sprite.scss'
    }));
    return spriteData.pipe(gulp.dest('src/scss'));
});


gulp.task('watchDev', function () {
    gulp.watch('src/scss/*.scss', ['gulpDev'])
});